[![ROS](https://upload.wikimedia.org/wikipedia/commons/b/bb/Ros_logo.svg)](http://www.ros.org/)

# Description
This package contains a bash script that helps building debian packages from ROS packages without using the ROS build farm.
It uses [bloom](http://wiki.ros.org/bloom).

Read the script for a description of the functions.

# Known caveats
- The script can't be run in parallel.
- Internet connection is mandatory if adding new `rosdep` keys because of the `rosdep update` call.

# Example usage
https://gitlab.com/InstitutMaupertuis/ros_additive_manufacturing/blob/melodic/release.bash

